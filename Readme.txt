The problem:

There's a river and on one side of the river we have both missionaries and cannibals who want to cross it with a boat.
The rules are the following:
1. In order to cross the river there must be at least one person in the boat
2. The maximum number of people the boat can carry accross the river is 3
3. Both on shore and in the boat there can never be more cannibals than missionaries because otherwise the cannibals will eat the missionaries - the same number of cannibals and missionaries is allowed, more missionaries than cannibals is also allowed.

Write a program which will print out (on screen) a step-by-step solution for how all the cannibals and missionaries can cross the river. The classical problem is with 3 missionaries and 3 cannibals. In this case a solution would be (the letter M represents a missionary, the letter C represents a cannibal, the symbol - represents the river):

1. MMMCCC -
2. MMM - CCC
3. MMMC - CC
4. C - MMMCC
5. CC - MMMC
6. - MMMCCC

State representation is arbitrary (you can use the one from the above example, show the position of the boat or not etc.). The program should at least solve the problem for 3 missionaries and 3 cannibals (the classical problem) - it would be best if it provided a solution in a general way for X missionaries and Y cannibals.

