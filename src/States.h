#ifndef STATES_H
#define STATES_H

#pragma once

using namespace std;
#include <iostream>

namespace CrossingRiver
{
	/**
	* Represents possible move
	*/
	class State
	{
	public:
		int _missioners;
		int _cannibals;
		bool _forward;

		State(int missioners, int cannibals, bool forward);
		bool operator == (const State &s) const;
	};

	/**
	* Represents the situation on both river sides
	*/
	class FullState
	{
	private:
		int _leftMissionaries;
		int _leftCannibals;
		int _rigthMissionaries;
		int _rigthCannibals;
		bool _boatOnLeft;

	public:
		FullState(int leftMissionaries, int leftCannibals,
			int rigthMissionaries, int rigthCannibals,
			bool boatOnLeft);

		bool  operator == (const FullState&  state) const;
		operator bool() const;
		bool Proceed(const State&  step);
		friend ostream& operator << (std::ostream& stream, const FullState& state);
	};
}

#endif //STATES_H
