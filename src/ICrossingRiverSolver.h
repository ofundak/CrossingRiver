#ifndef ICROSSINGRIVERSOLVER_H
#define ICROSSINGRIVERSOLVER_H

#pragma once

#include <memory>
using namespace std;

namespace CrossingRiver
{
	class ICrossingRiverSolver
	{
	public:
		virtual void Solve() = 0;
		virtual ~ICrossingRiverSolver() {}

		static shared_ptr<ICrossingRiverSolver> Create(unsigned int missioners, unsigned int cannibals);
	};
}

#endif // ICROSSINGRIVERSOLVER_H