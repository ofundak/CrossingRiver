// CrossingRiver.cpp : 
// This file contains the 'main' function. Program execution begins and ends there.
// Solves missioners and cannibals problem

#include <iostream>
#include <vector>
#include "ICrossingRiverSolver.h"

using namespace CrossingRiver;

int main()
{
	unsigned int cannibals;
	unsigned int missioners;
	
	cout << "Enter number of cannibals: " << endl;
	cin >> cannibals;
	cout << "Enter number of missioners: " << endl;
	cin >> missioners;

	shared_ptr<ICrossingRiverSolver> problem = ICrossingRiverSolver::Create(missioners, cannibals);
	problem->Solve();

	cout << endl << "Press any key to close." << endl;
	std::cin.get();
}
