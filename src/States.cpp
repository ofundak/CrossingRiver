#include "States.h"
#include <iomanip> 
#include <string>

using namespace CrossingRiver;

State::State(int missioners, int cannibals, bool forward) :
	_missioners(missioners),
	_cannibals(cannibals),
	_forward(forward)
{
}

bool State::operator == (const State &s) const
{
	return _missioners == s._missioners && _cannibals == s._cannibals && _forward == s._forward;
}

FullState::FullState(int leftMissionaries, int leftCannibals,
	int rigthMissionaries, int rigthCannibals,
	bool boatOnLeft) :
	_leftMissionaries(leftMissionaries),
	_leftCannibals(leftCannibals),
	_rigthMissionaries(rigthMissionaries),
	_rigthCannibals(rigthCannibals),
	_boatOnLeft(boatOnLeft)
{
}

bool FullState::operator == (const FullState&  state) const
{
	return    _leftMissionaries == state._leftMissionaries
		&& _leftCannibals == state._leftCannibals
		&& _rigthMissionaries == state._rigthMissionaries
		&& _rigthCannibals == state._rigthCannibals
		&& _boatOnLeft == state._boatOnLeft;
}

FullState::operator bool() const
{
	return  _leftCannibals >= 0
		&& _rigthCannibals >= 0
		&& (_leftMissionaries == 0 || _leftMissionaries >= _leftCannibals)
		&& (_rigthMissionaries == 0 || _rigthMissionaries >= _rigthCannibals);
}

bool FullState::Proceed(const State&  step)
{
	int  missionaries = step._missioners;
	int  cannibals = step._cannibals;

	if (step._forward)
	{
		if (!_boatOnLeft) return  false;
	}
	else
	{
		if (_boatOnLeft) return  false;
		missionaries *= -1;
		cannibals *= -1;
	}

	_boatOnLeft = !_boatOnLeft;
	_leftMissionaries -= missionaries;
	_rigthMissionaries += missionaries;

	_leftCannibals -= cannibals;
	_rigthCannibals += cannibals;

	return  operator bool();
}

ostream& CrossingRiver::operator << (std::ostream& stream, const FullState& state)
{
	int missioners = state._leftMissionaries + state._rigthMissionaries;
	int cannibals = state._leftCannibals + state._rigthCannibals;

	stream << setw(missioners) << std::string(state._leftMissionaries, 'M');
	stream << setw(cannibals) << std::string(state._leftCannibals, 'C');
	stream << "  -  ";
	stream << setw(missioners) << std::string(state._rigthMissionaries, 'M');
	stream << setw(cannibals) << std::string(state._rigthCannibals, 'C');
	stream << endl;

	return stream;
}
