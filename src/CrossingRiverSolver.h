#ifndef CROSSINGRIVERSOLVER_H
#define CROSSINGRIVERSOLVER_H

#pragma once
#include <vector>
#include <memory>
#include "States.h"
#include "ICrossingRiverSolver.h"

namespace CrossingRiver
{
	class NoSolutionSolver : public ICrossingRiverSolver
	{
	public:
		virtual void Solve() override;

		virtual ~NoSolutionSolver()
		{
		}
	};

	class CrossingRiverSolver : public ICrossingRiverSolver
	{
	private:
		unsigned int _missioners;
		unsigned int _cannibals;
		static const int BOAT_SEATS = 3;

		vector<State> _steps;
		FullState _endState;

	public:
		CrossingRiverSolver(unsigned int missioners, unsigned int cannibals);

		/**
		* Solves the missioners and cannibals problem
		*/
		virtual void Solve() override;

		virtual ~CrossingRiverSolver() {}

	private:
		/**
		* Fill the array of all possible moves
		*/
		void InitializeSteps();

		/**
		* Recursive method to iterate through possible steps
		*/
		bool Solve(vector<FullState>& history);

		/**
		* Solve the case when there are more missioners than cannibals.
		* The simpler solution exists for that case.
		*/
		void SolveMoreMissioners();

		/**
		* Solve the case when the number of missioners and cannibals are equal.
		*/
		void SolveEqualMissioners();

		/**
		* Print the found solution to the console
		*/
		void PrintSolution(const vector<FullState>& history);
	};
}

#endif //CROSSINGRIVERSOLVER_H
