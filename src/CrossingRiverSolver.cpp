#include "CrossingRiverSolver.h"
#include <algorithm> 

using namespace CrossingRiver;

shared_ptr<ICrossingRiverSolver> ICrossingRiverSolver::Create(unsigned int missioners, unsigned int cannibals)
{
	if (cannibals > missioners && missioners > 0)
	{
		return make_shared<NoSolutionSolver>();
	} else
		return make_shared<CrossingRiverSolver>(missioners, cannibals);

	// TODO Add new solver for case missioners > cannibals. This case can be solved by CrossingRiverSolver.
}


void NoSolutionSolver::Solve() 
{
	cout << "No soultion" << endl;
}

CrossingRiverSolver::CrossingRiverSolver(unsigned int missioners, unsigned int cannibals) :
		_missioners(missioners),
		_cannibals(cannibals),
	   _endState(0, 0, missioners, cannibals, false)
{
	InitializeSteps();
}

void CrossingRiverSolver::Solve()
{
	if (_cannibals == _missioners)
		SolveEqualMissioners();
	else
		SolveMoreMissioners();
}

void CrossingRiverSolver::InitializeSteps()
{
	_steps.clear();

	for (int boat = 0; boat <= 1; ++boat)
		for (int m = 0; m <= BOAT_SEATS; ++m)
			for (int c = 0; c <= BOAT_SEATS; ++c)
			{
				int sum = m + c;
				if (sum < 1 || sum > BOAT_SEATS || (c > m && m > 0))
					continue;

				_steps.push_back(State(m, c, boat));
			}
}

void CrossingRiverSolver::SolveMoreMissioners()
{
	SolveEqualMissioners();

	// TODO Implement new algorithm if we need to calculate faster
	// move 2 missioners and 1 cannibal to the right. If no more canibals put 1 missioner instead.
	// Move 1 missioner to the left.
	// Repeat until everyone is moved.

	// TODO refactor classes: Move new algorithm to different class + create ICrossingRiverSolver with factory method
}

void CrossingRiverSolver::SolveEqualMissioners()
{
	FullState startState(_missioners, _cannibals, 0, 0, true);
	vector<FullState> history;
	history.push_back(startState);

	if (!Solve(history))
	{
		cout << "No solution.";
	}
}

bool CrossingRiverSolver::Solve
(
	vector<FullState>& history
)
{
	if (history.back() == _endState)
	{
		PrintSolution(history);
		return true;
	}

	// Check whether we returned to the previous state
	if (history.size() > 1 && 
		find(rbegin(history) + 1, rend(history), history.back()) != rend(history))
			return false;

	for (const auto& s : _steps)
	{
		FullState nextState = history.back();
		if (!nextState.Proceed(s))
			continue;

		history.push_back(nextState);

		if (Solve(history))
			return true;
		else
			history.pop_back();
	}

	return false;
}

void CrossingRiverSolver::PrintSolution(const vector<FullState>& history)
{
	for (const auto& item : history)
		cout << item;
}

